
# qPDF Decode

[<img align="right" width="96" alt="Application icon depicting a white unlocked padlock on a red sheet of paper"
title="Link to qPDF Decode main repo"
src="img/qpdf-decode-gui.svg">](https://git.disroot.org/daltux/qpdf-decode)

Save a readable copy of your [PDF]() after unlocking it with your passphrase.

Main repository: <https://git.disroot.org/daltux/qpdf-decode>

## Dependencies

- **[QPDF](https://qpdf.sourceforge.io/)** tool
	- versions tested: 11.6.3, 8.0.2
- A [POSIX](https://pubs.opengroup.org/onlinepubs/9799919799/)-compliant [Shell](https://pubs.opengroup.org/onlinepubs/9799919799/utilities/V3_chap02.html) ([`/bin/sh`](https://pubs.opengroup.org/onlinepubs/9799919799/utilities/sh.html))
	- tested on [Debian GNU/Linux](https://debian.org) and Ubuntu with:
		- [`dash`](https://manpages.debian.org/testing/dash/dash.1.en.html) 0.5.12, 0.5.8
		- [`posh`](https://manpages.debian.org/testing/posh/posh.1.en.html) 0.14.1, 0.13.1
		- [`busybox sh`](http://www.busybox.net) 1.36.1
		- [ `bash`](https://www.gnu.org/software/bash/) 5.0.17
- If you would like to run the graphical user interface (`qpdf-decode-gui`):
	- [YAD](https://github.com/v1cont/yad)
		- Also, for installing it on the Applications menu of a desktop environment following [XDG](https://freedesktop.org) specifications:
			- [`xdg-desktop-menu`](https://portland.freedesktop.org/doc/xdg-desktop-menu.html) and [`xdg-icon-resource`](https://portland.freedesktop.org/doc/xdg-icon-resource.html) from **[`xdg-utils`](https://freedesktop.org/wiki/Software/xdg-utils/)**
		- tested on yad 0.40.0 from Debian, Xfce 4.18, GTK+ 3.24.38, also from Ubuntu 20.04, Unity 7.5.1., GTK 3.24.20 + 2.24.32
		- tested on yad 0.38.2 from Ubuntu 18.04

## Contents

Besides auxiliary files, this project consists of two shell scripts:

- **`qpdf-decode`** - main command-line interface that uses [QPDF](https://qpdf.sourceforge.io/)
- **`qdf-decode-gui`** - graphical user interface using [YAD](https://github.com/v1cont/yad) for creating dialogs

## Getting it (suggestion)

1. Check if the dependencies are available and install them if necessary.
	- Example on Debian: install package `qpdf`. For the GUI, install `yad`
	and, optionally, `xdg-utils` if you would like to set the desktop
	environment menu item.
		- Run as root: `apt install qpdf yad xdg-utils`
1. Clone the Git repository or decompress a zip/tar copy somewhere on your
filesystem.
	- Get e.g. [`main.tar.gz` from Disroot Forgejo](https://git.disroot.org/daltux/qpdf-decode/archive/main.tar.gz).
1. Check if you have permission to execute `qpdf-decode` and `qpdf-decode-gui`
and fix it if necessary.
	- Example: `chmod +x qpdf-decode qpdf-decode-gui`
1. Create [symbolic links](https://en.wikipedia.org/wiki/Symbolic_link) to
`qpdf-decode` and `qpdf-decode-gui` somewhere on your `PATH`, e.g.
`/usr/local/bin` (for all users) or your own `~/bin`
1. Run `qpdf-decode-gui --install` if you would like to set a desktop
environment applications menu item. If you run it as root, it will be
installed for all users, else only for the running user.
1. Check all the options by running `qpdf-decode --help` or
`qpdf-decode-gui --help`

## Using the GUI 🖱️

If you successfully installed the desktop applications menu item (`qpdf-decode-gui --install`), **qPDF Decode** should be available under categories "Office" or "Viewer". See file [`qpdf-decode-gui.desktop`](qpdf-decode-gui.desktop) for technical details.

When the GUI is started from the desktop menu or the command is directly called with no parameters (`qpdf-decode-gui`), a file selection dialog will be open on screen. Choose the PDF intended to be decrypted.

Another file selection dialog lets the user choose a destination file path and name where the decrypted version is going to be saved. If unset, default resulting file will be the same as the first one, adding a suffix `"_decrypted.pdf"`.

Next, a text input field will ask the file decryption passphrase.

The script will call `qpdf` to try decrypting the file and, if succeeded, you should be able to open the resulting file with the default PDF viewer or just end.

### Opening a file from another application

The main use case of this project, a personal problem the author would like to address, is saving decrypted versions of PDF invoices having password, often received by email. With all the tools we commonly have on our desktop environment, it is still a somewhat slow and error-prone procedure. qPDF Decode (GUI) is a direct attempt to make this situation easier.

qPDF Decode should be set on the desktop environment as a secondary application to open PDF files. This is easily done e.g. when you choose to open a PDF file with qPDF Decode for the first time, from an email, a file manager or other application. Just avoid setting it as the default application, which should continue to be a PDF viewer.

## Development

This project is still in early phase, but already functional. However, some development is yet to happen on important functionality it still misses such as localization All strings are hard coded in English.

If you would like to register any kind of feedback, please open an issue report on [Disroot Forgejo](https://git.disroot.org/daltux/qpdf-decode/issues).

Help is needed on testing the scripts on different environments. Please let us know if it works for you or not.

Shell scripting development itself is just a matter of using a plain text editor on almost the same execution environment as long as it is not too much bleeding-edge. Try to follow [POSIX](https://pubs.opengroup.org/onlinepubs/9799919799/) and use [ShellCheck](https://www.shellcheck.net/) to validate against common errors.

The project's [Disroot Forgejo Git repository](https://git.disroot.org/daltux/qpdf-decode) is where pull requests should be sent to as well.

## Copyright, licensing and disclaimer

[<img align="right" alt="GPLv3 - Free Software - Free as in Freedom"
title="GNU General Public License, version 3"
src="https://www.gnu.org/graphics/gplv3-with-text-136x68.png">](LICENSE.md)

Copyright © 2023,2024 [Daltux](https://daltux.net)

**qPDF Decode** is Free Software: you can redistribute it and/or modify
it under the terms of the [GNU General Public License](LICENSE.md) as
published by the [Free Software Foundation](https://fsf.org), either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
[GNU General Public License](LICENSE.md) for more details.

### Documentation license

[<img align="right" alt="GFDL logo"
title="GNU Free Documentation License, version 1.3"
src="https://www.gnu.org/graphics/gfdl-logo-tiny.png">](https://www.gnu.org/licenses/fdl.html)

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; see the
[license text](https://www.gnu.org/licenses/fdl.html) for details.

## Acknowledgements

- [QPDF](https://qpdf.sourceforge.io/) command-line tool used here for fast [PDF decryption](https://qpdf.readthedocs.io/en/stable/cli.html#option-decrypt)
- [YAD](https://github.com/v1cont/yad) GTK+ dialog boxes from the shell 
- [Neovim](https://neovim.io/) CLI text editor
- [Geany](https://geany.org/) IDE including a real-time Markdown preview [plugin](http://plugins.geany.org)
- [ShellCheck](https://shellcheck.net/) script static analysis tool
- [Papirus](https://git.io/papirus-icon-theme) SVG icon theme (GPLv3) providing elements mixed in `qpdf-decode-gui` icon
- [Inkscape](https://inkscape.org/) SVG editor
- [Debian](https://debian.org/) [GNU](https://gnu.org)/[Linux](https://kernel.org/linux.html) operating system

